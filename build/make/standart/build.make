core = 7.x
api = 2
projects[drupal][version] = 7.26

; Modules
projects[admin_menu][subdir] = contrib
projects[ctools][subdir] = contrib
projects[colorbox][subdir] = contrib
projects[views][subdir] = contrib
projects[views_mosaic][subdir] = contrib
projects[field_slideshow][subdir] = contrib
projects[token][subdir] = contrib
projects[jquery_update][subdir] = contrib
projects[devel][subdir] = contrib
